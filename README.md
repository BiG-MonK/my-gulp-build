# gulp-SukharevS

**Commands:**
- gulp ==> series(clean, parallel(htmlInclude, scripts, fonts, resources, imgToApp, svgSprites), fontsStyle, styles, watchFiles)

- build ==> series(clean, parallel(htmlInclude, scriptsBuild, fonts, resources, imgToApp, svgSprites), fontsStyle, stylesBuild, htmlMinify, tinypng)

- cache ==> series(cache, rewrite)

- deploy ==> deploy